import React, { useState } from "react";
import { Typography, Modal } from "antd";
import styled from "styled-components";

import AcceptForm from "components/moles/AcceptForm";

const { Title, Paragraph } = Typography;

const reasons = [
  {
    text:
      "1. Work with other cryptography and web3 experts who care about compassionate tech",
  },
  {
    text:
      "2. Taariq Lewis--a crypto OG, leader of SFCDEVs, hilarious, you gotta love him",
  },
  {
    text:
      "3. 3Box, Orchid, and ETH Foundation want to give you cash prizes. Let them!",
  },
  { text: "4. You're bored in your house and you need to stay home" },
  {
    text:
      "5. You're incredibly talented and you're bound to build something that strengthens the web3 ecosystem",
  },
];

export default function InviteCard(props) {
  const ButtonComponent = props.buttonComponent;

  const [showForm, setShowForm] = useState(false);

  function renderCard() {
    return (
      <Container>
        <Typography>
          <StyledTitle level={2}>You made it! 🎉</StyledTitle>
          <StyledParagraph>
            This is one of 50 invite links to this event. Learn more below and
            click join to accept your invitation when you're ready.
          </StyledParagraph>
        </Typography>
        <ButtonContainer>
          <ButtonComponent
            onClick={handleAccept}
            type="primary"
            shape="round"
            size="large"
          >
            Join
          </ButtonComponent>
          {/* <ButtonComponent style={{marginLeft: "10px"}} onClick={showReasons} type="secondary" shape="round" size="large">
          Top 5 Reasons to Join
        </ButtonComponent> */}
        </ButtonContainer>
      </Container>
    );
  }

  function handleAccept() {
    setShowForm(true);
  }

  function showReasons() {
    Modal.success({
      title: "Top 5 Reasons to Accept Your Invitation",
      content: renderReasons(),
    });
  }

  function renderReasons() {
    const list = reasons.map((text) => {
      return <div>{text}</div>;
    });
  }

  return (
    <>
      {!showForm && renderCard()}
      {showForm && (
        <AcceptForm
          buttonComponent={ButtonComponent}
          invitation={props.invitation}
        />
      )}
    </>
  );
}

const Container = styled.div`
  background: #19181a;
  border: none;
  color: white;
  padding: 20px;
  margin: 40px;
  max-width: 500px;
`;

const StyledTitle = styled(Title)`
  color: white !important;
`;

const StyledParagraph = styled(Paragraph)`
  color: white;
  font-family: "Open Sans";
  font-size: 1.2rem;
  margin-bottom: 2em !important;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
`;
