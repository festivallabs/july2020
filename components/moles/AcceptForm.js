import React, { useEffect, useState } from "react";
import { Form, Input as FormInput, Button, Typography } from "antd";
import styled from "styled-components";

import JoinDiscordButton from "components/atoms/JoinDiscordButton";
import FadeInDiv from "components/atoms/TimedFadeInDiv";
import { getFirebase } from "lib/firebase";
import * as colors from "theme/colors";
import LinkShareButton from "components/atoms/LinkShareButton";

const { Paragraph, Title } = Typography;

export default function AcceptForm(props) {
  const ButtonComponent = props.buttonComponent;

  const [showCopied, setShowCopied] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showError, setShowError] = useState(false);
  const [errorInfo, setErrorInfo] = useState({});
  const [showAlreadyAccepted, setShowAlreadyAccepted] = useState(false);

  useEffect(() => {
    if (props.invitation) {
      showIfAlreadyAcceptedInvite();
    }
  }, [props.invitation]);

  async function showIfAlreadyAcceptedInvite() {
    const firebase = await getFirebase();
    const db = firebase.firestore();

    let alreadyAccepted = false;
    let failure = false;

    await db
      .collection("invitations")
      .where("id", "==", props.invitation.id)
      .where("accepted", "==", false)
      .get()
      .then((snapshot) => {
        snapshot.forEach(() => {
          alreadyAccepted = true;
        });
      })
      .catch((err) => {
        console.error(err);
        failure = true;
      });

    setShowAlreadyAccepted(alreadyAccepted || failure);
  }

  async function onFinish(values) {
    if (props.invitation) {
      return await finishInvite(values);
    } else {
      return await finishReferral(values);
    }
  }

  async function finishInvite(values) {
    const firebase = await getFirebase();
    const db = firebase.firestore();

    const docRef = db.collection("invitations").doc(props.invitation.id);
    const regDocRef = db.collection("registrations").doc();

    await db
      .runTransaction(function (transaction) {
        // This code may get re-run multiple times if there are conflicts.
        return transaction.get(docRef).then(function (doc) {
          if (!doc.exists) {
            throw "Document does not exist!";
          }
          transaction.update(docRef, {
            accepted: true,
            updated: new Date().toISOString(),
          });
          transaction.set(regDocRef, {
            first_name: values.firstName,
            last_name: values.lastName,
            email: values.email,
            affiliations: values.affiliations,
            expertise: values.expertise,
            created: new Date().toISOString(),
            updated: new Date().toISOString(),
            invitation: db.doc("invitations/" + props.invitation.id),
          });
        });
      })
      .then(function () {
        console.log("Transaction successfully committed!");
        setShowSuccess(true);
      })
      .catch(function (error) {
        console.log("Transaction failed: ", error);
      });
  }

  async function finishReferral(values) {
    const firebase = await getFirebase();
    const db = firebase.firestore();

    const docRef = db.collection("referrals").doc(props.referral.id);
    const regDocRef = db.collection("registrations").doc();

    await db
      .runTransaction(function (transaction) {
        // This code may get re-run multiple times if there are conflicts.
        return transaction.get(docRef).then(function (doc) {
          if (!doc.exists) {
            throw "Document does not exist!";
          }
          transaction.set(regDocRef, {
            first_name: values.firstName,
            last_name: values.lastName,
            email: values.email,
            affiliations: values.affiliations,
            expertise: values.expertise,
            created: new Date().toISOString(),
            updated: new Date().toISOString(),
            referral: db.doc("referrals/" + props.referral.id),
          });
        });
      })
      .then(function () {
        console.log("Transaction successfully committed!");
        setShowSuccess(true);
      })
      .catch(function (error) {
        console.log("Transaction failed: ", error);
      });
  }

  function onFinishFailed(errorInfo) {
    setShowError(true);
    setErrorInfo(errorInfo);
    console.log("Failed:", errorInfo);
  }

  function renderForm() {
    return (
      <StyledForm
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Typography>
          <StyledTitle level={2}>Join</StyledTitle>
          <Heading>
            Join 49 other cryptography and web3 engineers from around the world
            to build solutions that increase privacy and autonomy online.
          </Heading>
        </Typography>

        <Form.Item
          name="firstName"
          noStyle={true}
          rules={[
            { max: 64, message: "Too long" },
            {
              required: true,
              message: "Please input your first name",
            },
          ]}
        >
          <Input
            placeholder={"First Name"}
            style={{ display: "inline-block", width: "calc(50% - 8px)" }}
          />
        </Form.Item>

        <Form.Item
          name="lastName"
          noStyle={true}
          rules={[
            { max: 64, message: "Too long" },
            {
              required: true,
              message: "Please input your last name",
            },
          ]}
        >
          <Input
            placeholder={"Last Name"}
            style={{
              display: "inline-block",
              width: "calc(50% - 8px)",
              margin: "0 8px",
            }}
          />
        </Form.Item>

        <Form.Item
          noStyle={true}
          name="email"
          rules={[
            { max: 80, message: "Too long" },
            { pattern: /^\S+@\S+\.\S+$/, message: "Malformed" },
            { required: true, message: "Please input your email address" },
          ]}
        >
          <Input placeholder={"email@protonmail.com"} />
        </Form.Item>

        <Form.Item
          noStyle={true}
          name="affiliations"
          rules={[{ max: 80, message: "Too long" }]}
        >
          <Input placeholder={"Company or University Affiliations"} />
        </Form.Item>

        <Form.Item
          noStyle={true}
          name="expertise"
          rules={[
            { max: 255, message: "Too long" },
            { required: true, message: "Should not be blank" },
          ]}
        >
          <InputTextArea
            style={{ minHeight: "100px" }}
            placeholder="Briefly describe your areas of expertise related to blockchain, cryptography, security or decentralized services."
          />
        </Form.Item>
        {showError && <div>{JSON.stringify(errorInfo["errorFields"])}</div>}
        <Form.Item>
          <ButtonComponent type="primary" htmlType="submit">
            Submit
          </ButtonComponent>
        </Form.Item>
      </StyledForm>
    );
  }

  function renderSuccess() {
    return (
      <>
        <Typography>
          <StyledTitle level={2}>You're in!</StyledTitle>
          <Heading>
            Event details will be sent to your inbox.
            <br />
            Feel free to invite teammates and friends.
          </Heading>
        </Typography>
        <div style={{ marginBottom: "30px" }}>
          <JoinDiscordButton innerButton={CTAButton} />
        </div>
        <LinkShareButton innerButton={CTAButton} text={"Invite Someone"} />
      </>
    );
  }

  function renderAlreadyAccepted() {
    return (
      <Typography>
        <StyledTitle level={2}>You're in!</StyledTitle>
        <Heading>You accepted your invitation.</Heading>
        <Paragraph>
          Please get in touch with us on Discord if you have questions.
        </Paragraph>
      </Typography>
    );
  }

  return (
    <FadeInDiv>
      {!showSuccess && !showAlreadyAccepted && renderForm()}
      {showSuccess && !showAlreadyAccepted && renderSuccess()}
      {showAlreadyAccepted && renderAlreadyAccepted()}
    </FadeInDiv>
  );
}

const StyledForm = styled(Form)`
  background: ${colors.BLACK};
  color: white;
  padding: 20px;
  margin: 40px;
  max-width: 500px;
`;

const StyledTitle = styled(Title)`
  color: white !important;
`;

const Heading = styled(Paragraph)`
  color: white;
  font-family: "Open Sans";
  font-size: 1.2rem;
  margin-bottom: 2em !important;
`;

const Input = styled(FormInput)`
  background: transparent;
  border: none;
  border-bottom: 1px solid white;
  color: white;
  font-family: "Open Sans";
  font-size: 1rem;
  margin-bottom: 40px;
  ::placeholder {
    color: gray;
    opacity: 1; /* Firefox */
  }
  :-ms-input-placeholder {
    /* Internet Explorer 10-11 */
    color: gray;
  }
  ::-ms-input-placeholder {
    /* Microsoft Edge */
    color: gray;
  }
`;

const InputTextArea = styled(Input.TextArea)`
  background: transparent;
  color: white;
  font-family: "Open Sans";
  font-size: 1rem;
  margin-top: 10px;
  margin-bottom: 40px;
  padding: 10px;
  ::placeholder {
    color: gray;
    opacity: 1; /* Firefox */
  }
  :-ms-input-placeholder {
    /* Internet Explorer 10-11 */
    color: gray;
  }
  ::-ms-input-placeholder {
    /* Microsoft Edge */
    color: gray;
  }
`;

const CTAButton = styled(Button)`
  align-items: center;
  border-radius: 24px !important;
  display: flex;
  font-size: 20px !important;
  height: 48px !important;
  padding: 6.4px 40px !important;
`;
