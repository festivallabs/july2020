import React, { useState } from "react";
import { Card, Typography } from "antd";
import styled from "styled-components";

import AcceptForm from "components/moles/AcceptForm";

const { Title, Paragraph } = Typography;

export default function ReferralCard(props) {
  const ButtonComponent = props.buttonComponent;

  const [showForm, setShowForm] = useState(false);

  function renderCard() {
    return (
      <Container>
        <Typography>
          <StyledTitle level={2}>You made it! 🎉</StyledTitle>
          <StyledParagraph>
            {props.referral.firstName.charAt(0).toUpperCase() +
              props.referral.firstName.slice(1)}{" "}
            invited you to hack. Learn more below and click join to accept your
            invitation when you're ready.
          </StyledParagraph>
        </Typography>
        <ButtonComponent
          onClick={handleAccept}
          type="primary"
          shape="round"
          size="large"
        >
          Join
        </ButtonComponent>
      </Container>
    );
  }

  function handleAccept() {
    setShowForm(true);
  }

  return (
    <>
      {!showForm && renderCard()}
      {showForm && (
        <AcceptForm
          buttonComponent={ButtonComponent}
          referral={props.referral}
        />
      )}
    </>
  );
}

const Container = styled.div`
  background: #19181a;
  border: none;
  color: white;
  padding: 20px;
  margin: 40px;
  max-width: 500px;
`;

const StyledTitle = styled(Title)`
  color: white !important;
`;

const StyledParagraph = styled(Paragraph)`
  color: white;
  font-family: "Open Sans";
  font-size: 1.2rem;
  margin-bottom: 2em !important;
`;
