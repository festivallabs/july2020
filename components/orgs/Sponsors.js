import React from "react";
import { Layout, Typography } from "antd";
import styled from "styled-components";

import FadeInDiv from "components/atoms/FadeInDiv";
import * as colors from "theme/colors";

const { Content } = Layout;
const { Title } = Typography;

export default function Sponsors() {
  return (
    <StyledContent>
      <Typography>
        <FadeInDiv>
          <StyledTitle>Technology Partners</StyledTitle>
        </FadeInDiv>
        <FadeInDiv>
          <Row>
            <Sponsor src="sponsor-3box.png" />
          </Row>
          <Row>
            <Sponsor src="sponsor-orchid.png" />
          </Row>
          <Row>
            <Sponsor src="sponsor-eth.png" />
          </Row>
          <Row>
            <Sponsor src="sponsor-thesis.jpg" />
          </Row>
        </FadeInDiv>
      </Typography>
    </StyledContent>
  );
}

const StyledContent = styled(Content)`
  background: ${colors.BACKGROUND_DARK};
  padding: 40px;
`;

const Row = styled.div`
  text-align: center;
  margin: auto;
  padding: 20px 0px;
  width: 100%;
`;

const Sponsor = styled.img`
  max-width: 500px;
  @media only screen and (max-width: 600px) {
    max-width: 300px;
  }
`;

const StyledTitle = styled(Title)`
  color: white !important;
  text-align: center;
`;
