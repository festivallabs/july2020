import React from "react";
import { Layout, Typography } from "antd";
import styled from "styled-components";

import FadeInDiv from "components/atoms/FadeInDiv";
import { BACKGROUND_DARK } from "theme/colors";

const { Content } = Layout;
const { Paragraph, Title } = Typography;

export default function Info() {
  return (
    <StyledContent>
      <div id="about"></div>
      <TextContainer>
        <Typography>
          <FadeInDiv>
            <Image src={"purpose.png"} />
            <StyledTitle>Purpose</StyledTitle>
            <StyledParagraph>
              A 48-hour virtual event for cryptography and web3 developers and
              researchers from across the world to
              <br />
              <br />
              (1) collaboratively contribute to software that increases social
              and economic autonomy, and
              <br />
              <br />
              (2) exchange feedback on eachother's work.
            </StyledParagraph>
          </FadeInDiv>

          <FadeInDiv>
            <Image src={"philosophy.png"} />
            <StyledTitle>Philosophy</StyledTitle>
            <StyledParagraph>
              Our philosophy is to design technology that increases the quality
              of life by increasing social and economic autonomy. This is
              compassionate technology. With decentralized services as the
              foundation, and cryptography as the connective tissue, we can
              build truly compassionate solutions to human problems.
            </StyledParagraph>
          </FadeInDiv>
          <FadeInDiv>
            <Image src={"experience.png"} />
            <StyledTitle>Experience</StyledTitle>
            <StyledParagraph>
              As a hacker, you will have the opportunity to join in highly
              technical discussions with developers from the most influential
              web3 and blockchain organizations in the world. They will
              brainstorm with you, answer technical questions, and provide
              feedback on your work.
              <br />
              <br />
              We've partnered with experts from a diverse array of fields who
              will provide insight and help pick the winning projects.
            </StyledParagraph>
          </FadeInDiv>

          <FadeInDiv>
            <Image src={"award.png"} />
            <StyledTitle>Awards</StyledTitle>
            <StyledParagraph>
              Partner organizations are offering awards for projects that make
              the best use of their technology in order to increase user
              autonomy on the web. Award winning teams will receive a cash prize
              valued in the range of $1,000 USD to $10,000 USD.
              <br />
              <br />A community award with a prize of $500 USD from each tech
              partner will be given to the team that develops the project that
              receives the most votes from all hackathon participants.
            </StyledParagraph>
            <div id="judges"></div>
          </FadeInDiv>
        </Typography>
      </TextContainer>
    </StyledContent>
  );
}

const Image = styled.img`
  height: 100px;
`;

const StyledFadeInDiv = styled(FadeInDiv)`
  margin-bottom: 30px;
`;

const StyledContent = styled(Content)`
  background: ${BACKGROUND_DARK};
`;

const SplitContent = styled(Content)`
  display: flex;
  flex-direction: row;
  min-height: 100vh;
`;

const TextContainer = styled.div`
  margin: auto;
  width: 50%;
  padding: 40px;
  @media only screen and (max-width: 600px) {
    width: auto;
  }
`;

const StyledTitle = styled(Title)`
  color: white !important;
`;

const StyledParagraph = styled(Paragraph)`
  color: white;
  font-size: 1.4rem;
  margin-bottom: 4em !important;
`;

const ImageContainer = styled.div`
  background-image: linear-gradient(rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.8)),
    url("https://i.pinimg.com/originals/f6/83/68/f683689acb8f3382022a27ceb5f66b3d.jpg");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  width: 50%;
`;
