import React from "react";
import { Avatar, Layout, Typography } from "antd";
import styled from "styled-components";

import FadeInDiv from "components/atoms/FadeInDiv";

import * as colors from "theme/colors";

const { Content } = Layout;
const { Paragraph, Title } = Typography;

const judges = [
  { name: "Lindsay Nuon", bio: "Google | EmpirEqual", image: "lindsay.jpg" },
  { name: "Luis Cuende", bio: "Aragon | Stampery", image: "luis.jpg" },
  { name: "Taariq Lewis", bio: "Promise Protocols", image: "taariq.jpg" },
  { name: "Vanessa Grellet", bio: "Consensys", image: "vanessa.jpg" },
];

export default function Judges() {
  function renderJudges() {
    return judges.map((judge) => {
      return (
        <Judge key={judge.name}>
          <StyledAvatar src={judge.image} />
          <Typography>
            <JudgeName level={4}>{judge.name}</JudgeName>
            <JudgeBio>{judge.bio}</JudgeBio>
          </Typography>
        </Judge>
      );
    });
  }

  return (
    <StyledContent>
      <Typography>
        <FadeInDiv>
          <StyledTitle>Knowledge Partners</StyledTitle>
        </FadeInDiv>
      </Typography>
      <FadeInDiv>
        <ContentRow>{renderJudges()}</ContentRow>
      </FadeInDiv>
      <div id="sponsors"></div>
    </StyledContent>
  );
}

const StyledContent = styled(Content)`
  background: ${colors.BACKGROUND_DARK};
  padding: 40px;
`;

const StyledTitle = styled(Title)`
  color: white !important;
  text-align: center;
`;

const ContentRow = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
  padding: 40px;
`;

const Judge = styled.div`
  align-items: center;
  color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  max-width: 200px;
  text-align: center;
`;

const JudgeName = styled(Title)`
  color: white !important;
  padding-top: 8px;
`;

const JudgeBio = styled(Paragraph)`
  color: white;
  font-size: 1.2rem;
  padding-bottom: 30px;
`;

const StyledAvatar = styled(Avatar)`
  height: 20vh;
  width: 20vh;
`;
