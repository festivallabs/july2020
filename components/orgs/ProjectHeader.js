import React from "react";
import { PageHeader, Layout, Menu } from "antd";
import Link from "next/link";
import styled from "styled-components";

import { APPLICATION_URL } from "lib/links";
import * as colors from "theme/colors";
import * as fonts from "theme/fonts";

const { Header: LayoutHeader } = Layout;

export default function ProjectHeader(props) {
  return (
    <StyledHeader>
      <Link href="/">
        <Hyperlink>
          <StyledImage src="../logo.png" />
          <HeaderText>Festival Hackathon</HeaderText>
        </Hyperlink>
      </Link>
    </StyledHeader>
  );
}

const StyledHeader = styled(LayoutHeader)`
  background: black;
  color: white;
  position: sticky;
  top: 0;
  width: 100%;
  z-index: 1;
`;

const Hyperlink = styled.span`
  :hover {
    cursor: pointer;
  }
`;

const StyledImage = styled.img`
  height: 40px;
`;

const HeaderText = styled.span`
  font-family: ${fonts.DEFAULT};
  font-size: 1rem;
  font-weight: lighter;
  letter-spacing: 2px;
  margin-left: 12px;
  text-transform: uppercase;
`;
