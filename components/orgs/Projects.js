import React from "react";
import { Layout, Typography } from "antd";
import Link from "next/link";
import styled from "styled-components";

import FadeInDiv from "components/atoms/FadeInDiv";
import projects from "lib/projects";
import * as colors from "theme/colors";

const { Content } = Layout;
const { Title, Paragraph } = Typography;

const PROJECT_SLUGS = Object.keys(projects);

export default function Projects() {
  function renderProjects() {
    return PROJECT_SLUGS.map((slug) => {
      return (
        <Row key={slug}>
          <Link href={`/july2020/${slug}`}>
            <Image src={`team-${slug}.png`} />
          </Link>
          <StyledParagraph>{projects[slug].name}</StyledParagraph>
        </Row>
      );
    });
  }

  return (
    <StyledContent id="submissions">
      <TextContainer>
        <Typography>
          <FadeInDiv>
            <StyledTitle>Amazing Results</StyledTitle>
          </FadeInDiv>
          <FadeInDiv>{renderProjects()}</FadeInDiv>
        </Typography>
      </TextContainer>
    </StyledContent>
  );
}

const StyledContent = styled(Content)`
  background: ${colors.BACKGROUND_DARK};
  padding: 40px;
`;

const TextContainer = styled.div`
  margin: auto;
  width: 50%;
  padding: 40px;
  @media only screen and (max-width: 600px) {
    width: auto;
  }
`;

const Row = styled.div`
  text-align: center;
  margin: auto;
  padding: 20px 0px;
  width: 100%;
`;

const Image = styled.img`
  max-width: 500px;
  :hover {
    cursor: pointer;
  }
  @media only screen and (max-width: 600px) {
    max-width: 300px;
  }
`;

const StyledTitle = styled(Title)`
  color: white !important;
  text-align: center;
`;

const StyledParagraph = styled(Paragraph)`
  color: white;
  font-size: 1.4rem;
  margin-bottom: 4em !important;
  text-align: center;
`;
