import React from "react";
import styled from "styled-components";

import BitBucket from "components/svg/BitBucket";
import Discord from "components/svg/Discord";
import * as colors from "theme/colors";
import * as fonts from "theme/fonts";

const socials = [
  {
    key: "discord",
    label: "Connect with us",
    icon: Discord,
    href: "https://discord.gg/K4Qk339",
    target: "_blank",
  },
  {
    key: "bitbucket",
    label: "View source code",
    icon: BitBucket,
    href: "https://bitbucket.org/festivallabs/july2020/src/master/",
    target: "_blank",
  },
];

export default function Footer() {
  function renderSocials() {
    return socials.map((social) => {
      return (
        <SocialLink
          key={social.key}
          href={social.href}
          target={social.target}
          rel="noreferrer"
        >
          <SocialIconContainer>
            <SocialIcon>
              <social.icon />
            </SocialIcon>
            {social.label}
          </SocialIconContainer>
        </SocialLink>
      );
    });
  }
  return (
    <StyledFooter>
      Festival Labs &copy; 2020
      {renderSocials()}
    </StyledFooter>
  );
}

const StyledFooter = styled.div`
  align-items: center;
  font-family: ${fonts.DEFAULT};
  font-weight: lighter;
  letter-spacing: 0.1em;
  background: black;
  color: rgba(255, 255, 255, 0.65);
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  padding: 30px;
  text-align: center;
  text-transform: uppercase;
`;

const SocialIconContainer = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

const SocialLink = styled.a`
  color: white;
  margin: 10px;
  width: 200px;
  :hover {
    color: ${colors.BLUE};
  }
`;

const SocialIcon = styled.div`
  height: 24px;
  margin-right: 20px;
  width: 24px;
`;
