import React from "react";
import { Button } from "antd";
import styled from "styled-components";

import InviteCard from "components/moles/InviteCard";
import ReferralCard from "components/moles/ReferralCard";
import TimedFadeInDiv from "components/atoms/TimedFadeInDiv";
import TelegramShareButton from "components/atoms/TelegramShareButton";

import { APPLICATION_URL } from "lib/links";
import * as colors from "theme/colors";
import * as fonts from "theme/fonts";

export default function Hero(props) {
  function renderContent() {
    if (props.invitation) {
      if (props.invitation.accepted) {
        return (
          <>
            <div style={{ color: "white" }}>
              This invite link has already been used!
            </div>
            {renderCTA()}
          </>
        );
      }
      return (
        <InviteCard invitation={props.invitation} buttonComponent={CTAButton} />
      );
    } else if (props.referral) {
      return (
        <ReferralCard referral={props.referral} buttonComponent={CTAButton} />
      );
    } else {
      return renderCTA();
    }
  }

  function renderCTA() {
    return (
      <CTAContainer>
        <CTAButton type="primary" shape="round" size="large">
          <a href={APPLICATION_URL} target="_blank" rel="noreferrer">
            Apply
          </a>
        </CTAButton>
        <TelegramShareButton innerButton={CTAButton} />
      </CTAContainer>
    );
  }

  function renderProjectsCTA() {
    return (
      <CTAContainer>
        <CTAButton type="primary" shape="round" size="large">
          <a href="#submissions">View Submissions</a>
        </CTAButton>
      </CTAContainer>
    );
  }

  return (
    <>
      <BackgroundImage></BackgroundImage>
      <TimedFadeInDiv>
        <Container>
          <TitleContainer>
            <TitleTextContainer>
              <TitleTop>Festival</TitleTop>
              <TitleBottom>Hackathon</TitleBottom>
              <Date>July 10 - 12</Date>
            </TitleTextContainer>
          </TitleContainer>
          <SubTitle>
            Compassionate technology for socioeconomic autonomy
          </SubTitle>
          <div id="accept"></div>
          {renderProjectsCTA()}
        </Container>
      </TimedFadeInDiv>
    </>
  );
}

const BackgroundImage = styled.div`
  background-image: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 1)),
    url("hero.gif");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 100vh;
  width: 100%;
  position: absolute;
  z-index: 0;
`;

const Container = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-bottom: 40px;
  margin-top: 20px;
  min-height: 100vh;
`;

const TitleContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const TitleTextContainer = styled.div`
  margin-bottom: 60px;
`;

const TitleTop = styled.div`
  color: white;
  font-family: ${fonts.DEFAULT};
  font-size: 4.8rem;
  letter-spacing: 0.05em;
  line-height: 1;
  text-align: center;
  text-transform: uppercase;
  @media only screen and (max-width: 600px) {
    font-size: 3.4rem;
  }
`;

const TitleBottom = styled.div`
  color: white;
  font-family: "Work Sans", sans-serif;
  font-size: 2.8rem;
  font-weight: 100;
  letter-spacing: 0.22em;
  line-height: 1;
  padding-left: 4px;
  padding-top: 10px;
  padding-bottom: 16px;
  text-align: center;
  text-transform: uppercase;
  border-bottom: solid 6px ${colors.ORANGE};
  @media only screen and (max-width: 600px) {
    font-size: 2.2rem;
  }
`;

const Date = styled.div`
  color: white;
  font-family: "Open Sans", sans-serif;
  font-size: 4rem;
  font-weight: lighter;
  text-align: center;
  text-transform: uppercase;
`;

const SubTitle = styled.p`
  color: white;
  font-family: "Open Sans";
  font-size: 24px;
  font-weight: 100;
  text-align: center;
`;

const CTAContainer = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
  width: 330px;
`;

const CTAButton = styled(Button)`
  align-items: center;
  border-radius: 24px !important;
  display: flex;
  font-size: 20px !important;
  height: 48px !important;
  justify-content: space-between;
  padding: 6.4px 40px !important;
`;
