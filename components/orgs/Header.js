import React from "react";
import { Layout, Menu } from "antd";
import styled from "styled-components";

import { APPLICATION_URL } from "lib/links";
import * as colors from "theme/colors";
import * as fonts from "theme/fonts";

const { Header: LayoutHeader } = Layout;

export default function Header(props) {
  const menuItems = [
    { key: 0, label: "Submissions", href: "#submissions", highlight: true },
    { key: 1, label: "About", href: "#about" },
    { key: 2, label: "Judges", href: "#judges" },
    { key: 3, label: "Sponsors", href: "#sponsors" },
  ];

  // if (props.invitation || props.referral) {
  //   menuItems.push({ key: 4, label: "Join", href: "#accept", highlight: true });
  // } else {
  //   menuItems.push({
  //     key: 4,
  //     label: "Join",
  //     href: APPLICATION_URL,
  //     highlight: true,
  //     target: "_blank",
  //   });
  // }

  function renderMenuItems() {
    return menuItems.map((item) => {
      const style = {
        fontFamily: "Open Sans",
        fontWeight: "lighter",
        letterSpacing: "0.1em",
      };
      if (item.highlight == true) {
        style["color"] = "rgb(0, 98, 174)";
      }
      return (
        <MenuItem key={item.key}>
          <a
            href={item.href}
            target={item.target}
            rel="norefererr"
            style={style}
          >
            {item.label}
          </a>
        </MenuItem>
      );
    });
  }

  return (
    <StyledHeader>
      <StyledMenu theme="dark" mode="horizontal" defaultSelectedKeys={[]}>
        {renderMenuItems()}
      </StyledMenu>
    </StyledHeader>
  );
}

const StyledHeader = styled(LayoutHeader)`
  background: black;
  position: sticky;
  top: 0;
  width: 100%;
  z-index: 1;
`;

const StyledMenu = styled(Menu)`
  background: transparent !important;
  float: right;
  font-family: ${fonts.DEFAULT};
  font-size: 1rem;
  font-weight: lighter;
  text-transform: uppercase;
`;

const MenuItem = styled(Menu.Item)`
  padding-left: 12px;
  padding-right: 12px;
`;
