import React from "react";
import { Avatar, Layout, Typography } from "antd";
import styled from "styled-components";

import FadeInDiv from "components/atoms/FadeInDiv";
import GitHub from "components/svg/GitHub";
import VideoCamera from "components/svg/VideoCamera";
import Web from "components/svg/Link";
import { BACKGROUND_DARK } from "theme/colors";
import * as colors from "theme/colors";

const { Content } = Layout;
const { Paragraph, Title } = Typography;

const LINK_ICONS = {
  github: <GitHub />,
  web: <Web />,
  video: <VideoCamera />,
};

export default function ProjectInfo(props) {
  const linkKeys = Object.keys(props.links);

  function renderLinks() {
    return linkKeys.map((item) => {
      const linkIcon = LINK_ICONS[item];
      return (
        <SocialLink
          key={item}
          href={props.links[item]}
          target="_blank"
          rel="noreferrer"
        >
          <SocialIconContainer>
            <SocialIcon>{linkIcon}</SocialIcon>
            {item}
          </SocialIconContainer>
        </SocialLink>
      );
    });
  }

  function renderTeam() {
    return (
      <FadeInDiv>
        <StyledTitle>Team</StyledTitle>
        <StyledParagraph>{renderTeamItems()}</StyledParagraph>
      </FadeInDiv>
    );
  }

  function renderTeamItems() {
    return props.team.map((item, idx) => {
      return <div key={idx}>{item.name}</div>;
    });
  }

  function renderAwards() {
    if (props.awards.length > 0) {
      return (
        <FadeInDiv>
          <Image src="../winner.png" />
          <StyledTitle>Awards</StyledTitle>
          <StyledParagraph>{renderAwardItems()}</StyledParagraph>
        </FadeInDiv>
      );
    }
  }

  function renderAwardItems() {
    return props.awards.map((item, idx) => {
      return <div key={idx}>{item.partner} Award</div>;
    });
  }

  function renderImages() {
    return props.images.map((item, idx) => {
      return <ImageLarge key={idx} src={`../team-${props.slug}-${item}.png`} />;
    });
  }

  return (
    <StyledContent>
      <TextContainer>
        <Typography>
          <FadeInDiv>
            <StyledTitle>{props.name}</StyledTitle>
          </FadeInDiv>
          <FadeInDiv>
            <LinksContainer>{renderLinks()}</LinksContainer>
          </FadeInDiv>
          <FadeInDiv>
            <StyledParagraph>
              {props.description}
              <br />
              <ImageLarge src={`../team-${props.slug}.png`} />
              {renderImages()}
            </StyledParagraph>
          </FadeInDiv>
          {renderTeam()}
          {renderAwards()}
        </Typography>
      </TextContainer>
    </StyledContent>
  );
}

const StyledAvatar = styled(Avatar)`
  height: 20vh;
  width: 20vh;
`;

const Image = styled.img`
  height: 100px;
`;

const ImageLarge = styled.img`
  margin-top: 20px;
  max-width: 500px;
  @media only screen and (max-width: 600px) {
    max-width: 300px;
  }
`;

const StyledFadeInDiv = styled(FadeInDiv)`
  margin-bottom: 30px;
`;

const StyledContent = styled(Content)`
  background: ${BACKGROUND_DARK};
`;

const SplitContent = styled(Content)`
  display: flex;
  flex-direction: row;
  min-height: 100vh;
`;

const TextContainer = styled.div`
  margin: auto;
  width: 50%;
  padding: 40px;
  @media only screen and (max-width: 600px) {
    width: auto;
  }
`;

const LinksContainer = styled.div`
  color: white;
  display: flex;
  flex-direction: row;
  :hover {
    color: ${colors.BLUE};
  }
`;

const StyledTitle = styled(Title)`
  color: white !important;
`;

const StyledParagraph = styled(Paragraph)`
  color: white;
  font-size: 1.4rem;
  margin-bottom: 4em !important;
`;

const SocialIconContainer = styled.div`
  align-items: center;
  color: white;
  display: flex;
  flex-direction: row;
  justify-content: center;
  :hover {
    color: ${colors.BLUE};
  }
`;

const SocialLink = styled.a`
  color: white;
  margin: 10px;
`;

const SocialIcon = styled.div`
  height: 24px;
  margin-right: 20px;
  width: 24px;
`;
