import React from "react";
import styled from "styled-components";

import Footer from "components/orgs/Footer";
import ProjectHeader from "components/orgs/ProjectHeader";
import ProjectInfo from "components/orgs/ProjectInfo";

export default function Project(props) {
  return (
    <Container>
      <ProjectHeader />
      <ProjectInfo {...props} />
      <Footer></Footer>
    </Container>
  );
}

const Container = styled.div``;
