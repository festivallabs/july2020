import React from "react";
import styled from "styled-components";

import Footer from "components/orgs/Footer";
import Header from "components/orgs/Header";
import Hero from "components/orgs/Hero";
import Info from "components/orgs/Info";
import Judges from "components/orgs/Judges";
import Projects from "components/orgs/Projects";
import Sponsors from "components/orgs/Sponsors";

export default function Landing(props) {
  return (
    <Container>
      <Hero
        invitation={props.invitation}
        referral={props.referral}
        reasons={props.reasons}
      />
      <Header invitation={props.invitation} referral={props.referral} />
      <Projects />
      <Info />
      <Judges />
      <Sponsors />
      <Footer></Footer>
    </Container>
  );
}

const Container = styled.div``;
