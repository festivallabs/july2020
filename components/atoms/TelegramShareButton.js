import React from "react";
import { TelegramShareButton as ShareButton } from "react-share";
import styled from "styled-components";

import Telegram from "components/svg/Telegram";

export default function TelegramShareButton(props) {
  const InnerButton = props.innerButton;
  return (
    <ShareButton title={"Festival Hackathon"} url="https://hack.fstvl.io">
      <InnerButton shape="round" size="large">
        {(props.text && props.text) || "Share"}
        <StyledTelegramIcon width={24} height={24} />
      </InnerButton>
    </ShareButton>
  );
}

const StyledTelegramIcon = styled(Telegram)`
  background: transparent;
  padding-left: 6px;
`;
