import React, { useState } from "react";
import { Typography } from "antd";
import styled from "styled-components";

const { Paragraph } = Typography;

export default function LinkShareButton(props) {
  const [showLink, setShowLink] = useState(false);
  const InnerButton = props.innerButton;

  function handleClick() {
    setShowLink(true);
  }

  function renderText() {
    if (showLink) {
      return <StyledParagraph copyable>https://hack.fstvl.io</StyledParagraph>;
    } else {
      return (props.text && props.text) || "Share";
    }
  }
  return (
    <InnerButton shape="round" size="large" onClick={handleClick}>
      {renderText()}
    </InnerButton>
  );
}

const StyledParagraph = styled(Paragraph)`
  margin-bottom: 0px !important;
`;
