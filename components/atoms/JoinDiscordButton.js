import React from "react";
import styled from "styled-components";

import Discord from "components/svg/Discord";

export default function JoinDiscordButton(props) {
  const CTAButton = props.innerButton;
  return (
    <div style={{ width: "230px" }}>
      <CTAButton
        shape="round"
        size="large"
        type="primary"
        href="https://discord.gg/K4Qk339"
        target="_blank"
        rel="noreferrer"
      >
        Join Discord
        <StyledDiscordIcon color={"white"} height={24} width={24} />
      </CTAButton>
    </div>
  );
}

const StyledDiscordIcon = styled(Discord)`
  padding-left: 6px;
  margin-left: 6px;
`;
