import Head from "next/head";
import Landing from "components/templates/Landing";
import { getFirebase } from "lib/firebase";

function Invite(props) {
  return (
    <div className="container">
      <Head>
        <title>Festival Hackathon | July 10 - 12, 2020</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="Autonomy. Compassion. Cryptography. Web3."
        />
        {/* Social */}
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Festival Hackathon | July 10 - 12 | Web3 &amp; Cryptography"
        />
        <meta
          property="og:description"
          content="This is a your personal invitation to build compassionate technology for socioeconomic autonomy."
        />
        <meta
          property="og:image"
          content="https://hack.fstvl.io/share_card-twitter-invite.png"
        />
        <meta property="og:image:width" content="1012" />
        <meta property="og:image:height" content="506" />
        <meta property="og:url" content="https://hack.fstvl.io/invite" />
        <meta
          name="twitter:title"
          content="Festival Hackathon | July 10 - 12 | Web3 &amp; Cryptography"
        />
        <meta
          name="twitter:description"
          content="This is a your personal invitation to build compassionate technology for socioeconomic autonomy."
        />
        <meta
          name="twitter:image"
          content="https://hack.fstvl.io/share_card-twitter-invite.png"
        />
        <meta
          name="twitter:image:alt"
          content="You've been tapped for the cryptography and web3 hackathon"
        />
        <meta name="twitter:card" content="summary_large_image" />
        {/* Non-essential 
          <meta property="og:site_name" content="European Travel, Inc.">
          <meta name="twitter:site" content="@website-username">
        */}
        <script src="/static/widgetbotcrate.js" />
      </Head>
      <Landing
        invitation={props.invitation}
        referral={props.referral}
        reasons={true}
      />
      <style jsx global>
        {`
          @import url("https://fonts.googleapis.com/css2?family=Yantramanav:wght@100;400;500;700&display=swap");
          @import url("https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;800&display=swap");
          @import url("https://fonts.googleapis.com/css2?family=Work+Sans:wght@100;400;700&display=swap");
          html,
          body {
            background: black;
          }
          @media only screen and (max-width: 600px) {
            html {
              font-size: 14px;
            }
          }
        `}
      </style>
    </div>
  );
}

Invite.getInitialProps = async (ctx) => {
  let invitation = null;
  let referral = null;

  const firebase = await getFirebase();
  const db = firebase.firestore();

  await db
    .collection("invitations")
    .where("code", "==", ctx.query.code || null)
    .get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        invitation = {};
        invitation["id"] = doc.id;
        invitation["code"] = doc.data().code;
        invitation["accepted"] = doc.data().accepted;
      });
    })
    .catch((err) => {
      console.log(err);
    });

  await db
    .collection("referrals")
    .where("code", "==", ctx.query.ref || null)
    .get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        referral = {};
        referral["id"] = doc.id;
        referral["code"] = doc.data().code;
        referral["firstName"] = doc.data().first_name;
      });
    })
    .catch((err) => {
      console.log(err);
    });

  return { invitation, referral };
};

export default Invite;
