export default {
  "secretsmarket": {
    name: "Secrets Market",
    description: "Wikileaks on steroids. Allows people to post secrets (designs, contracts, leaks, code) that others can buy escrow style.",
    links: {
      web: "https://www.secrestmkt.com",
      github: "https://github.com/SecretsMarket",
      video: "https://www.youtube.com/watch?v=MxVySLrFrzU",
    },
    images: ["screenshot"],
    team: [
      {name: "Mike"},
      {name: "Cynthia"},
      {name: "Sajida"}
    ],
    awards: [{partner: "Community"}, {partner: "3Box 1st Place"}, {partner: "Orchid"}],
  },
  "benotify": {
    name: "BeNotify: Beacon Notifier",
    description: "Validator notifications for eth 2.0",
    links: {
      web: "https://t.me/beacon_notify_bot",
      github: "https://github.com/princesinha19/beacon-notifier",
      video: "https://youtu.be/nM_FMuRo39s",
    },
    images: [],
    team: [
      {name: "Prince"},
      {name: "Ayush"}
    ],
    awards: [
      {partner: "Ethereum Foundation"}
    ],
  },
  "blackmirror": {
    name: "BlackMirror",
    description: "The roleplaying immersive game to foster empathy and inclusion in the workplace and daily life.",
    links: {
      github: "https://github.com/rachit2501/Black-Mirror",
    },
    images: ["screenshot"],
    team: [
      {name: "Alex"},
      {name: "Craig"},
      {name: "Yutong"},
      {name: "Rachit"},
    ],
    awards: [
      {partner: "3Box 2nd Place"},
      {partner: "Community"},
    ],
  },
  "tbtc4u": {
    name: "tbtc4u",
    description: "Watching over tBTC to help those in need - free proofs for all!",
    links: {
      github: "https://github.com/prestwich/tbtc4u",
    },
    images: [],
    team: [
      {name: "James"},
      {name: "Mark"},
    ],
    awards: [
      {partner: "Thesis Runner Up"},
    ],
  },
  "plebtc": {
    name: "pleBTC",
    description: "A protocol for porting BTCs to the ethereum network via a token. Yet another /[a-z]BTC/ token.",
    links: {
      github: "https://github.com/corollari/pleBTC",
    },
    images: [],
    team: [
      {name: "Corollari"},
    ],
    awards: [
      {partner: "Thesis 1st Place"},
    ],
  },
  "thebeehive": {
    name: "The Bee Hive",
    description: "Supply Chain for Honey. Help artisan honey producers with the traceability of their products to help with quality control and delivery.",
    links: {
      github: "https://github.com/edsphinx/the-bee-hive",
    },
    images: [],
    team: [
      {name: "edsphinx"},
    ],
    awards: [],
  },
}
