import Head from "next/head";
import Landing from "components/templates/Landing";

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Festival Hackathon | July 10 - 12, 2020</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="At Festival Hackathon the top web3 and cryptography developers build compassionate technologies."
        />
        {/* Social */}
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Festival Hackathon | July 10 - 12 | Web3 &amp; Cryptography"
        />
        <meta
          property="og:description"
          content="Join us in building compassionate technology for socioeconomic autonomy."
        />
        <meta
          property="og:image"
          content="https://hack.fstvl.io/share_card-instagram.png"
        />
        <meta property="og:url" content="https://hack.fstvl.io" />
        <meta
          name="twitter:title"
          content="Festival Hackathon | July 10 - 12 | Web3 &amp; Cryptography"
        />
        <meta
          name="twitter:description"
          content="Join us in building compassionate technology for socioeconomic autonomy."
        />
        <meta
          name="twitter:image"
          content="https://hack.fstvl.io/share_card-twitter.png"
        />
        <meta name="twitter:card" content="summary_large_image" />
        {/* Non-essential 
          <meta property="og:site_name" content="European Travel, Inc.">
          <meta name="twitter:image:alt" content="Alt text for image">
          <meta name="twitter:site" content="@website-username">
        */}
        <script src="/static/widgetbotcrate.js" />
      </Head>
      <Landing />
      <style jsx global>
        {`
          @import url("https://fonts.googleapis.com/css2?family=Yantramanav:wght@100;400;500;700&display=swap");
          @import url("https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;800&display=swap");
          @import url("https://fonts.googleapis.com/css2?family=Work+Sans:wght@100;400;700&display=swap");
          html,
          body {
            background: black;
          }
          @media only screen and (max-width: 600px) {
            html {
              font-size: 14px;
            }
          }
        `}
      </style>
    </div>
  );
}
