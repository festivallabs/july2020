import App from "next/app";
import * as Sentry from "@sentry/browser";

import "antd/dist/antd.css";
import "components/atoms/FadeInDiv.css";

if (process.env.NODE_ENV == "production") {
  Sentry.init({ dsn: process.env.SENTRY_DSN });
}

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  state = {
    loading: true,
    opacity: 1,
  };

  componentDidMount() {
    this.setState({ opacity: 0 });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 300);
  }

  componentDidCatch(error, errorInfo) {
    Sentry.withScope((scope) => {
      Object.keys(errorInfo).forEach((key) => {
        scope.setExtra(key, errorInfo[key]);
      });

      Sentry.captureException(error);
    });

    super.componentDidCatch(error, errorInfo);
  }

  render() {
    const { Component, pageProps } = this.props;

    return <Component {...pageProps} />;
  }
}

export default MyApp;
