export const BACKGROUND_DARK = "#19181A";
export const BACKGROUND_HEADER = "#1E0B02";
export const BLACK = "#141414";
export const WHITE = "#FDFEF8";
export const BLUE = "#008fff";
export const ORANGE = "#de8868";
